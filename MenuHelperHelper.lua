if MenuHelperHelper then return end

MenuHelperHelper = {}

local function LogError(er)
	log('[MenuHelperHelper] Error in '.. (debug.getinfo(2).name or '(unknown)') .. '(): '..(er or 'unknown'))
end

MenuHelperHelper._tweaks = {
	default = {
		priority = nil,  -- The higher this is, the higher the item is in the menu
		localized = nil,  -- If localized string should be used (if not you'll see IDs like 'my_table_set2')
		slider_min = nil,  -- Minimum value for sliders
		slider_max = nil,  -- Maximum value for sliders
		show_value = nil,  -- If slider should display the number value set on it
		float = nil,   -- If true, allows the slider to save values with floating point (otherwise they are rounded down)
		divider = nil, -- If false, divider formatted string ('_ 20') will be treated as a normal string
		callback = nil,  -- Sets the function to call when the value is changed (a function value)
		ignore = nil, -- If true, a menu item won't be created
		
		-- These are only for 'default'
		save_only_changed = nil, -- If true, settings set as default won't be saved to the save file
		instant_callback = nil -- If true, callback is called after every change
	}
}

local _functions = {} -- Callbacks for buttons, MenuCallbackHandler:MHH_function() calls these
local _callbacks = {} -- Main callbacks that are called when the settings get saved
local _defaults = {} -- Default values (original tables) if save_only_changed=true

function MenuHelperHelper:HasMenu(menu_id)
	return _callbacks[menu_id] and true or false
end

function MenuHelperHelper:CreateMenu(settings_table, menu_id, savefile_name, set_clbk, loc_file)
	if type(settings_table) ~= 'table' then
		LogError('First argument must be a table of settings, got \''..type(settings_table)..'\'')
		return
	end
	
	if type(menu_id) ~= 'string' then
		LogError('Second argument must be the text ID of the new menu, got \''..type(menu_id)..'\'')
		return
	end
	
	if savefile_name and type(savefile_name) ~= 'string' then
		savefile_name = menu_id
	end
	
	if _callbacks[menu_id] then
		LogError('Menu with such ID already exists: \''..type(menu_id)..'\'')
		return
	end
	
	MenuHelperHelper._tweaks = MenuHelperHelper._tweaks or {}
	if (MenuHelperHelper._tweaks['default'] or {}).save_only_changed then
		_defaults[menu_id] = deep_clone(settings_table)
	end
	
	MenuHelperHelper:LoadSettings(savefile_name, settings_table)
	
	_callbacks[menu_id] = set_clbk or (function() log('[MenuHelperHelper] No callback set for menu \''..menu_id..'\'') end)
	
	MenuHelperHelper:CreateCallbacks(menu_id, settings_table, savefile_name)
	
	local menus = MenuHelperHelper:GetMenus(settings_table, menu_id, loc_file)
	
	Hooks:Add("MenuManagerSetupCustomMenus", "MenuHelperHelper_SetupCustomMenus_"..menu_id, function( menu_manager, nodes )
		for k,v in pairs(menus) do
			MenuHelper:NewMenu(k)
		end
	end)
	
	Hooks:Add("MenuManagerPopulateCustomMenus", "MenuHelperHelper_PopulateCustomMenus_"..menu_id, function( menu_manager, nodes )
		for k,v in pairs(menus) do
			for k2,v2 in pairs(v) do
				MenuHelperHelper:AddMenuItem(v2)
			end
			
			-- Normal BLT does not set 'localize_help' to match 'localize'
			if not BLTSuperMod then
				local menu = MenuHelper:GetMenu(k)
				for k2,v2 in pairs(menu._items_list) do
					v2:set_parameter('localize_help', v2:parameter('localize'))
				end
			end
		end
	end)
	
	Hooks:Add("MenuManagerBuildCustomMenus", "MenuHelperHelper_BuildCustomMenus_"..menu_id, function( menu_manager, nodes )
		for k,v in pairs(menus) do
			nodes[k] = MenuHelper:BuildMenu(k, v.build_data)
		end
		MenuHelper:AddMenuItem(nodes["blt_options"], menu_id, menu_id, menu_id..'_desc')
	end)
	
	MenuHelperHelper._tweaks = {}
end

function MenuHelperHelper:GetMenus(tbl, menu_id, loc_file)
	local menus = {}
	
	local loc = nil
	if type(loc_file) == 'string' then
		loc = {}
	end
	
	local function AddItems(name, item, last_menu_name, fullname)
		local title_id = fullname and (fullname:gsub(':','_')..'_'..name) or name
		
		MenuHelperHelper._tweaks = MenuHelperHelper._tweaks or {}
		local tweaks = MenuHelperHelper._tweaks[name] or MenuHelperHelper._tweaks['default'] or {}
		
		if tweaks.ignore then return end
		
		if tweaks.localized == nil then
			tweaks.localized = true
		end
		
		if loc and tweaks.localized then
			loc[#loc+1] = ' '
			loc[#loc+1] = title_id
			loc[#loc+1] = title_id..'_desc'
		end
		
		if type(tweaks.callback) == 'function' then
			_functions[fullname .. ':' .. name] = tweaks.callback
		end
		
		if type(item) == 'table' then
			if not item[1] then -- If it's an array then they want a multiple choice
				fullname = fullname and fullname .. ':' .. name or name
				
				menus[title_id] = {}
				
				for k,v in pairs(item) do
					AddItems(k,v,title_id,fullname)
				end
				
				if last_menu_name then
					menus[last_menu_name][name] = {
						type = 'button',
						id = name,
						title = tweaks.localized and title_id or name,
						desc = tweaks.localized and title_id..'_desc' or name,
						next_node = title_id,
						menu_id = last_menu_name,
						priority = tweaks.priority,
						localized = tweaks.localized
					}
				else
					menus[title_id]['build_data'] = { back_callback = 'MHH_save_' .. menu_id }
				end
				
				return
			else
				local val = item.value or 1
				local items = deep_clone(item)
				items.value = nil
				for k,v in pairs(items) do
					items[k] = title_id..'_'..v
					if loc then
						loc[#loc+1] = title_id..'_'..v
					end
				end
				menus[last_menu_name][name] = {
					type = 'multiple_choice',
					id = fullname .. ':' .. name,
					title = tweaks.localized and title_id or name,
					description = tweaks.localized and title_id..'_desc' or name,
					callback = tweaks.callback and 'MHH_function' or 'MHH_multiple_' .. menu_id,
					items = items,
					default_value = 1,
					value = val,
					menu_id = last_menu_name,
					priority = tweaks.priority,
					localized = tweaks.localized
				}
			
				return
			end
		end
		
		if type(item) == 'number' then
			menus[last_menu_name][name] = {
				type = 'slider',
				id = fullname .. ':' .. name,
				title = tweaks.localized and title_id or name,
				description = tweaks.localized and title_id..'_desc' or name,
				callback = tweaks.callback and 'MHH_function' or (tweaks.float and 'MHH_float_' .. menu_id or 'MHH_number_' .. menu_id),
				value = item,
				menu_id = last_menu_name,
				min = tweaks.slider_min or 0,
				max = tweaks.slider_max or 50,
				step = tweaks.slider_step or 1,
				show_value = tweaks.show_value,
				localized = tweaks.localized,
				priority = tweaks.priority
			}
			
			return
		end
		
		if type(item) == 'boolean' then
			menus[last_menu_name][name] = {
				type = 'toggle',
				id = fullname .. ':' .. name,
				title = tweaks.localized and title_id or name,
				description = tweaks.localized and title_id..'_desc' or name,
				callback = tweaks.callback and 'MHH_function' or 'MHH_boolean_' .. menu_id,
				value = item,
				menu_id = last_menu_name,
				localized = tweaks.localized,
				priority = tweaks.priority
			}
			
			return
		end
		
		if type(item) == 'string' then
			local div = item:match('^_ (%d+)$')
			if div and tweaks.divider ~= false then
				menus[last_menu_name][name] = {
					type = 'divider',
					id = fullname .. ':' .. name,
					size = tonumber(div),
					menu_id = last_menu_name,
					priority = tweaks.priority
				}
			else
				menus[last_menu_name][name] = {
					type = 'input',
					id = fullname .. ':' .. name,
					title = tweaks.localized and title_id or name,
					description = tweaks.localized and title_id..'_desc' or name,
					callback = tweaks.callback and 'MHH_function' or 'MHH_string_' .. menu_id,
					value = item,
					menu_id = last_menu_name,
					localized = tweaks.localized,
					priority = tweaks.priority
				}
			end
			
			return
		end
		
		if type(item) == 'function' then
			local id = fullname .. ':' .. name
			
			_functions[id] = item
			
			menus[last_menu_name][name] = {
				type = 'button',
				id = id,
				title = tweaks.localized and title_id or name,
				description = tweaks.localized and title_id..'_desc' or name,
				callback = 'MHH_function',
				menu_id = last_menu_name,
				localized = tweaks.localized,
				priority = tweaks.priority
			}
			
			return
		end
		
		LogError('Could not add this field to the menu: \'' .. fullname .. ':' .. name .. '\'')
	end
	
	AddItems(menu_id, tbl)
	
	if loc then
		local file = io.open(SavePath .. loc_file, "w")
		if file then
			local text = '{'
			for k,v in pairs(loc) do
				if v == ' ' then
					text = text..'\n'
				else
					text = text..'    "'..v..'": " ",\n'
				end
			end
			text = text:sub(1,text:len() - 2)..'\n}'
			file:write(text)
			file:close()
		else
			LogError('Failed to write localization file')
		end
	end
	
	return menus
end

function MenuHelperHelper:SetValue(id, value, separator)
	separator = separator or ':'
	local path,name = string.match(id, '(.+)'..separator..'(.-)$')
	if path and name then
		local menu = MenuHelper:GetMenu(path:gsub(separator, '_'))
		if menu then
			for k,v in pairs(menu._items_list or {}) do
				if v._parameters.name == id then
					if type(value) == 'boolean' then
						menu._items_list[k]:set_value(value and 'on' or 'off')
					else
						menu._items_list[k]:set_value(value)
					end
					
					local clbk = menu._items_list[k]._parameters.callback_name
					clbk = clbk and clbk[1] or nil
					if clbk and MenuCallbackHandler[clbk] then
						MenuCallbackHandler[clbk](nil, menu._items_list[k])
					end
					return
				end
			end
			LogError('Could not find menu item: \''.. id .. '\'')
		else
			LogError('Could not find menu: \''..path..'\'  separator: \''..separator..'\'')
		end
	else
		LogError('Unable to parse ID: \''..id..'\'  separator: \''..separator..'\'')
	end
end

function MenuHelperHelper:CreateCallbacks(menu_id, settings_table, savefile_name)

	local function SetInTable(path, tbl, value)
		local function returnNew(tbl2, inx, right_path)
			local new_tbl = {}
			
			inx = inx + 1
			
			for k,v in pairs(tbl2) do
				if type(v) == 'table' then
					new_tbl[k] = returnNew(v, inx, (right_path and path[inx] == k) and true or false)
				else
					if right_path and path[inx] == k then
						new_tbl[k] = value
					else
						new_tbl[k] = v
					end
				end
			end
			
			return new_tbl
		end
	
		return returnNew(tbl,1,true)
	end
	
	MenuHelperHelper._tweaks = MenuHelperHelper._tweaks or {}
	local instant_callback = (MenuHelperHelper._tweaks['default'] or {}).instant_callback
	local save_only_changed = (MenuHelperHelper._tweaks['default'] or {}).save_only_changed
	
	MenuCallbackHandler['MHH_boolean_' .. menu_id] = function(self, item)
		local index = item._parameters.name
		local split = string.split(index, ':')
		local val = item:value() == 'on'
		settings_table = SetInTable(split, settings_table, val)
		
		if instant_callback then
			_callbacks[menu_id](settings_table, menu_id)
		end
	end
	
	MenuCallbackHandler['MHH_number_' .. menu_id] = function(self, item)
		local index = item._parameters.name
		local split = string.split(index, ':')
		local val = math.floor(item:value())
		settings_table = SetInTable(split, settings_table, val)
		
		if instant_callback then
			_callbacks[menu_id](settings_table, menu_id)
		end
	end
	
	MenuCallbackHandler['MHH_float_' .. menu_id] = function(self, item)
		local index = item._parameters.name
		local split = string.split(index, ':')
		local val = item:value()
		settings_table = SetInTable(split, settings_table, val)
		
		if instant_callback then
			_callbacks[menu_id](settings_table, menu_id)
		end
	end
	
	MenuCallbackHandler['MHH_multiple_' .. menu_id] = function(self, item)
		local index = item._parameters.name .. ':value'
		local split = string.split(index, ':')
		local val = item:value()
		settings_table = SetInTable(split, settings_table, val)
		
		if instant_callback then
			_callbacks[menu_id](settings_table, menu_id)
		end
	end
	
	MenuCallbackHandler['MHH_string_' .. menu_id] = function(self, item)
		local index = item._parameters.name
		local split = string.split(index, ':')
		local val = item:value()
		settings_table = SetInTable(split, settings_table, val)
		
		if instant_callback then
			_callbacks[menu_id](settings_table, menu_id)
		end
	end
	
	function MenuCallbackHandler:MHH_function(item)
		local index = item._parameters.name
		if _functions[index] then
			_functions[index](item)
			
			if instant_callback then
				_callbacks[menu_id](settings_table, menu_id)
			end
			
			return
		end
		LogError('Callback does not exist for button: \''..index..'\'')
	end
	
	MenuCallbackHandler['MHH_save_' .. menu_id] = function(self, item)
		local to_save
		if save_only_changed then
			local function compareAndReturn(tbl1, tbl2)
				if not (tbl1 and tbl2) then
					return tbl1 or tbl2
				end
				local newtbl = {}
				for k,v in pairs(tbl1) do
					if type(v) == 'table' then
						newtbl[k] = compareAndReturn(v, tbl2[k])
					elseif v == tbl2[k] then
						newtbl[k] = nil
					else
						newtbl[k] = tbl2[k]
					end
				end
				return newtbl
			end
			to_save = compareAndReturn(_defaults[menu_id], settings_table)
		end
		
		to_save = to_save or settings_table
	
		MenuHelperHelper:SaveSettings(savefile_name, to_save)
		
		_callbacks[menu_id](settings_table, menu_id)
	end
end

function MenuHelperHelper:LoadSettings(filename, settings_table)
	if not filename then return end
	
	local file = io.open(SavePath .. filename, "r")
	if file then
		
		local function Merge(tbl1, tbl2)
			for k2,v2 in pairs(tbl2) do
				local key = k2:match('^%d+$') and tonumber(k2) or k2
				
				if tbl1[key] ~= nil then
					if type(v2) == 'table' then
						tbl1[key] = Merge(tbl1[key], v2)
					else
						tbl1[key] = v2
					end
				end
			end
			return tbl1
		end

		settings_table = Merge(settings_table, json.decode(file:read("*all")) or {})
		
		file:close()
	end
end

function MenuHelperHelper:SaveSettings(filename, settings_table)
	if not filename then return end
	
	local file = io.open(SavePath .. filename, "w")
	if file then
		file:write(json.encode(settings_table))
		file:close()
	else
		LogError('Failed to write file to save settings')
	end
end

function MenuHelperHelper:AddMenuItem(data)
	if not data.type then return end

	local type = data.type
	local id = data.id
	local title = data.title
	local desc = data.description
	local callback = data.callback
	local priority = data.priority
	local value = data.value
	local localized = data.localized
	local menu_id = data.menu_id
	
	if type == "button" then
		MenuHelper:AddButton({
			id = id,
			title = title,
			desc = desc,
			callback = callback,
			next_node = data.next_menu or data.next_node or nil,
			menu_id = menu_id,
			priority = priority,
			localized = localized,
		})
		return
	end

	if type == "toggle" then
		MenuHelper:AddToggle({
			id = id,
			title = title,
			desc = desc,
			callback = callback,
			value = value,
			menu_id = menu_id,
			priority = priority,
			localized = localized,
		})
		return
	end

	if type == "slider" then
		MenuHelper:AddSlider({
			id = id,
			title = title,
			desc = desc,
			callback = callback,
			value = value,
			min = data.min or 0,
			max = data.max or 1,
			step = data.step or 0.1,
			show_value = true,
			menu_id = menu_id,
			priority = priority,
			localized = localized,
		})
		return
	end

	if type == "divider" then
		MenuHelper:AddDivider({
			id = "divider_" .. menu_id .. "_" .. id,
			size = data.size,
			menu_id = menu_id,
			priority = priority,
		})
		return
	end

	if type == "keybind" then
		local key = ""
		if data.keybind_id then
			local mod = BLT.Mods:GetModOwnerOfFile( file_path )
			if mod then
				local params = {
					id = data.keybind_id,
					allow_menu = data.run_in_menu,
					allow_game = data.run_in_game,
					show_in_menu = data.show_in_menu,
					name = title,
					desc = desc,
					localize = true,
					callback = data.func and MenuCallbackHandler[data.func],
				}
				BLT.Keybinds:register_keybind( mod, params )
			end

			local bind = BLT.Keybinds:get_keybind( data.keybind_id )
			key = bind and bind:Key() or ""
		end

		MenuHelper:AddKeybinding({
			id = id,
			title = title,
			desc = desc,
			connection_name = data.keybind_id,
			button = key,
			binding = key,
			menu_id = menu_id,
			priority = priority,
			localized = localized,
		})
		return
	end

	if type == "multiple_choice" then
		MenuHelper:AddMultipleChoice({
			id = id,
			title = title,
			desc = desc,
			callback = callback,
			items = data.items,
			value = value,
			menu_id = menu_id,
			priority = priority,
			localized = localized,
		})
		return
	end

	if type == "input" then
		MenuHelper:AddInput({
			id = id,
			title = title,
			desc = desc,
			callback = callback,
			value = value,
			menu_id = menu_id,
			priority = priority,
			localized = localized,
		})
		return
	end
	
	LogError('Unknown item type: \''..type..'\'')
end